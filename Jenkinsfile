import java.text.SimpleDateFormat
// CONSTANTS
def SERVICE_TAG
def CREDENTIAL_API
def LOAD_BALANCER_API
def SERVICE_ENV_API
def DOCKER_IMAGE

def TARGET_BRANCH = [
    "master": [
        "type": "server_instance_no",
        "value": 3372446
    ]
]

def image_tag
def container_image

def slackNotifier(String buildResult) {
    if ( buildResult == "SUCCESS" ) {
        slackSend color: "good", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was successful (<${env.BUILD_URL}|Open>)"
    }
    else if( buildResult == "FAILURE" ) {
        slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was failed (<${env.BUILD_URL}|Open>)"
    }
    else if( buildResult == "UNSTABLE" ) {
        slackSend color: "warning", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was unstable (<${env.BUILD_URL}|Open>)"
    }
    else if( buildResult == "ABORTED" ) {
        slackSend color: "warning", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was skipped (<${env.BUILD_URL}|Open>)"
    }
    else {
        slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} its result was unclear (<${env.BUILD_URL}|Open>)"
    }
}

pipeline {
    agent any
    stages {
        stage('Variable Initialization'){
            steps {
                script {
                    withCredentials([
                        string(credentialsId: 'credential_api', variable: 'credential_api'),
                        string(credentialsId: 'load_balancer_api', variable: 'load_balancer_api'),
                        string(credentialsId: 'service_env_api', variable: 'service_env_api')
                    ]) {
                        CREDENTIAL_API = credential_api
                        LOAD_BALANCER_API = load_balancer_api
                        SERVICE_ENV_API = service_env_api
                        SERVICE_TAG = 'MARKER'
                        DOCKER_IMAGE = "danodocker/marker"
                    }

                    echo message: "CREDENTIAL_API -> ${CREDENTIAL_API}"
                    echo message: "LOAD_BALANCER_API -> ${LOAD_BALANCER_API}"
                    echo message: "SERVICE_ENV_API -> ${SERVICE_ENV_API}"
                    echo message: "SERVICE_TAG -> ${SERVICE_TAG}"
                    echo message: "DOCKER_IMAGE -> ${DOCKER_IMAGE}"

                    if (!TARGET_BRANCH.containsKey(BRANCH_NAME)) {
                        currentBuild.result = 'ABORTED'
                        error('Stopping early…')
                    }
                }
            }
        }

        stage('Docker Image Build'){
            when {
                expression { return TARGET_BRANCH.containsKey(BRANCH_NAME) }
            }
            steps {
                script {
                    echo message: "BRANCH_NAME -> ${BRANCH_NAME}"

                    def dateFormat = new SimpleDateFormat("yyyy-MM-dd")
                    def date = new Date()

                    image_tag = dateFormat.format(date)
                    sh "curl -f ${String.format(SERVICE_ENV_API, SERVICE_TAG, BRANCH_NAME)} > .env"
                    sh "cat .env"

                    withDockerRegistry(credentialsId: 'danodocker') {
                        sh "docker build --target prod -t ${DOCKER_IMAGE}:${BRANCH_NAME}-${image_tag} ."
                        container_image = docker.image("${DOCKER_IMAGE}:${BRANCH_NAME}-${image_tag}")
                    }
                }
            }
        }

        stage('Docker Image Push'){
            when {
                expression { return TARGET_BRANCH.containsKey(BRANCH_NAME) }
            }
            steps {
                script {
                    withDockerRegistry(credentialsId: 'danodocker') {
                        container_image.push()
                        container_image.push(BRANCH_NAME)
                    }
                }
            }
        }

        stage('Docker Container Switch'){
            when {
                expression { return TARGET_BRANCH.containsKey(BRANCH_NAME) }
            }
            steps {
                script {
                    def server_instance_nos = []

                    if ( TARGET_BRANCH[BRANCH_NAME]['type'] == "server_instance_no" ) {
                        server_instance_nos += TARGET_BRANCH[BRANCH_NAME]['value']
                    }
                    else if (TARGET_BRANCH[BRANCH_NAME]['type'] == "load_balancer_no") {
                        def servers_str = sh returnStdout: true, script: "curl ${String.format(LOAD_BALANCER_API, TARGET_BRANCH[BRANCH_NAME]['value'])}"
                        def servers = readJSON text: servers_str

                        for (server in servers) {
                            server_instance_nos += server['_id']
                        }
                    }

                    for ( instance_no in server_instance_nos) {
                        def ips_str = sh returnStdout: true, script: "curl ${CREDENTIAL_API}/${instance_no}"
                        def credential = readJSON text: ips_str

                        echo message: "ips_str -> ${ips_str}"

                        credential.name = "remote"
                        credential.allowAnyHosts = true

                        sshCommand remote: credential, command: "./deploy.sh"
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                slackNotifier(currentBuild.currentResult)
            }
            cleanWs()
        }
    }
}
