import "reflect-metadata";

import * as Koa from "koa";
import * as winston from "winston";

import * as middleware from "./middleware";
import { logger } from "./logger";

export async function createApp(appName: string): Promise<Koa<Koa.DefaultState, Koa.MarkerContext>> {
  const app = new Koa<Koa.DefaultState, Koa.MarkerContext>() as Koa<Koa.DefaultState, Koa.MarkerContext> ;
  app.context.appName = appName;

  app.use(logger(winston));
  app.use(middleware.apiStatusCheck);

  const db = await import("./model");
  const controller = await import("./controller");
  const job = await import("./job");

  const mongoose = await db.initApp(app);
  await job.initApp(mongoose.connection.db);
  return controller.initApp(app, middleware.apiKeyAuthentication);
}
