import * as Koa from 'koa';
import * as winston from 'winston';
import * as moment from "moment";

import { getRandomString } from "./common";


export function logger(winstonInstance: typeof winston) {
  winstonInstance.configure({
    level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
    transports: [
      new winston.transports.Console({
          format: winston.format.printf(
            info => `${moment().format('YYYY-MM-DDTHH:mm:ss')} [${info.level.toLocaleUpperCase().padEnd(5)}] ${info.uuid} ${info.path} :  ${info.message}`
          )
        } as winston.transports.ConsoleTransportOptions
      )
    ]
  } as winston.LoggerOptions);

  return async(ctx: Koa.MarkerContext, next: Koa.Next) => {
    let logLevel: string = 'info';
    const rStr = getRandomString();

    const msg: winston.LogEntry = {
      level: logLevel,
      message: '',
      path: ctx.originalUrl,
      uuid: rStr
    };

    msg.message = `<--  ${ctx.method}`;
    winstonInstance.log(msg);

    const start = new Date().getTime();
    const handler = {
      get: (target, name) => function (...messages: Array<any>) {
        if(!(name in winstonInstance)) return;

        let message: string = '';
        for(let m of messages){
          switch (typeof m) {
            case 'undefined':
              break;
            case 'object':
              message += JSON.stringify(m);
              break;
            default:
              message += m;
          }
        }

        const msg = {
          message: message,
          path: ctx.originalUrl,
          uuid: rStr
        };
        winstonInstance[name](msg)
      }
    };
    ctx.log = new Proxy({}, handler);

    await next();

    const ms = new Date().getTime() - start;

    if (ctx.status >= 500) {
      logLevel = 'error';
    } else if (ctx.status >= 400) {
      logLevel = 'warn';
    } else if (ctx.status >= 100) {
      logLevel = 'info';
    }
    msg.level = logLevel;

    msg.message = `-->  ${ctx.method} ${ctx.status} ${ms}ms`;
    winstonInstance.log(msg);
  };
}