import { model, Schema, Types, Document, Model, SaveOptions } from "mongoose";
import { Schedule, ISchedule, IScheduleTemplate } from "./Schedule";
import { Group, IGroupDocument, IGroupTemplate } from "./Group";
import { GroupCreatePayload } from "../common";
import { isEqual } from "lodash";
import { IAction } from "./Action";

export interface IStrategyTemplate {
  isActivated?: boolean;
  shortDesc?: string;
  longDesc?: string;
}

export interface IStrategyDocument extends IStrategyTemplate, Document {
  isActivated: boolean;
  schedules: Types.DocumentArray<ISchedule>;
  groups: Array<Types.ObjectId | IGroupDocument>;

  createGroup(param: GroupCreatePayload): Promise<IGroupDocument>;
  getGroups(): Promise<Array<IGroupDocument>>;
  isInMyGroups(group: Types.ObjectId | IGroupDocument): Promise<boolean>;
  addSchedule(...schedules: Array<IScheduleTemplate>): Promise<ISchedule[]>;
  activate(): Promise<IStrategyDocument>;
  deactivate(): Promise<IStrategyDocument>;
}

export interface IStrategyModel extends Model<IStrategyDocument> {
  pageSize: number;

  create(
    docs: IStrategyTemplate[],
    callback?: (err: any, res: IStrategyDocument[]) => void,
  ): Promise<IStrategyDocument[]>;
  create(
    docs: IStrategyTemplate[],
    options?: SaveOptions,
    callback?: (err: any, res: IStrategyDocument[]) => void,
  ): Promise<IStrategyDocument[]>;
  create(...docs: IStrategyTemplate[]): Promise<IStrategyDocument>;
  create(...docsWithCallback: IStrategyTemplate[]): Promise<IStrategyDocument>;

  findByPage(
    pageNum?: number,
    ...conditions: Array<any>
  ): Promise<IStrategyDocument[]>;
  getSchedule(id: Types.ObjectId, sid: Types.ObjectId): Promise<ISchedule>;
  getAction(
    id: Types.ObjectId,
    sid: Types.ObjectId,
    aid: Types.ObjectId,
  ): Promise<IAction>;
}

const StrategySchema: Schema = new Schema({
  isActivated: {
    type: Boolean,
    required: true,
    default: false,
  },
  schedules: {
    type: [Schedule],
    default: [],
  },
  groups: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: "Group",
      },
    ],
    default: [],
  },
  shortDesc: { type: String },
  longDesc: { type: String },
});

StrategySchema.statics.pageSize = 10;
StrategySchema.statics.findByPage = async function(
  pageNum: number = 1,
  ...conditions: Array<any>
) {
  const cls: IStrategyModel = this;
  return cls
    .find(...conditions)
    .limit(cls.pageSize)
    .skip(cls.pageSize * (pageNum - 1))
    .exec();
};

StrategySchema.statics.getSchedule = async function(
  id: Types.ObjectId,
  sid: Types.ObjectId,
): Promise<ISchedule> {
  const cls: IStrategyModel = this;

  const s = await cls.findById(id);
  if (!s) throw new Error(`No matched strategy to id ${id}`);

  const sc = s.schedules.find(v => isEqual(v._id, sid));
  if (!sc) throw new Error(`No matched schedule to id ${sid}`);

  return sc;
};

StrategySchema.statics.getAction = async function(
  id: Types.ObjectId,
  sid: Types.ObjectId,
  aid: Types.ObjectId,
): Promise<IAction> {
  const cls: IStrategyModel = this;

  const s = await cls.findById(id);
  if (!s) throw new Error(`No matched strategy to id ${id}`);

  const sc = s.schedules.find(v => isEqual(v._id, sid));
  if (!sc) throw new Error(`No matched schedule to id ${sid}`);

  const a = sc.actions.find(v => isEqual(v._id, aid));
  if (!a) throw new Error(`No matched action to id ${aid}`);

  return a;
};

/**
 * @define Create new group and push self.groups
 * @param priority (optional, default is 0 define priority of group)
 * @param meta (optional, default is [] define meta data for event hook)
 * @param shortDesc (optional, short description of new group)
 * @param longDesc (optional, long description of new group)
 * @return new group
 */
StrategySchema.methods.createGroup = async function({
  priority,
  meta,
  shortDesc,
  longDesc,
}: GroupCreatePayload): Promise<IGroupDocument> {
  const self: IStrategyDocument = this;

  const newGroup = await Group.create({
    strategy: self._id,
    priority: priority,
    meta: meta,
    shortDesc: shortDesc,
    longDesc: longDesc,
  } as IGroupTemplate);

  self.groups.push(newGroup);
  await self.save();

  return newGroup;
};

/**
 * @define Get populated groups
 * @return Array of group instance
 */
StrategySchema.methods.getGroups = async function(): Promise<
  Array<IGroupDocument>
> {
  const self: IStrategyDocument = this;

  if (self.groups.length === 0) return self.groups as Array<IGroupDocument>;
  else if (self.groups[0] instanceof Group)
    return self.groups as Array<IGroupDocument>;

  await self.populate("groups").execPopulate();
  return self.groups as Array<IGroupDocument>;
};

StrategySchema.methods.isInMyGroups = async function(
  group: Types.ObjectId | IGroupDocument,
): Promise<Boolean> {
  const self: IStrategyDocument = this;
  const groups = await self.getGroups();
  const groupId: Types.ObjectId =
    group instanceof Group
      ? (group as IGroupDocument)._id
      : (group as Types.ObjectId);

  return groups.some(g => isEqual(g._id, groupId));
};

/**
 * @define Add schedule to schedules
 * @param schedules (... Schedule template objects)
 * @return Array of group instance
 */
StrategySchema.methods.addSchedule = async function(
  ...schedules: Array<IScheduleTemplate>
): Promise<ISchedule[]> {
  const self: IStrategyDocument = this;

  const re = await Promise.all(
    schedules.map(async v => self.schedules.create(v)),
  );
  self.schedules.push(...re);
  await self.save();

  if (self.isActivated)
    await Promise.all(self.schedules.map(v => v.scheduleJob(self._id)));

  return re;
};

StrategySchema.methods.activate = async function() {
  const self: IStrategyDocument = this;

  if (self.isActivated) return self;

  await Promise.all(self.schedules.map(v => v.scheduleJob(self._id)));

  self.isActivated = true;
  return self.save();
};

StrategySchema.methods.deactivate = async function() {
  const self: IStrategyDocument = this;

  if (!self.isActivated) return self;

  await Promise.all(self.schedules.map(v => v.unScheduleJob(self._id)));

  self.isActivated = false;
  return self.save();
};

export const Strategy = model<IStrategyDocument, IStrategyModel>(
  "Strategy",
  StrategySchema,
);
