import { Document, model, Model, SaveOptions, Schema, Types } from "mongoose";
import { isEqual } from "lodash";
import { EventHookMeta } from "../common";

export interface IGroupTemplate {
  strategy: Types.ObjectId;
  priority?: number;
  meta?: Array<EventHookMeta>;
  shortDesc?: string;
  longDesc?: string;
}

export interface IGroupDocument extends IGroupTemplate, Document {
  priority: number;
  meta: Types.Array<EventHookMeta>;

  compare(g: IGroupDocument): IGroupDocument;
}

export interface IGroupModel extends Model<IGroupDocument> {
  create(
    docs: IGroupTemplate[],
    callback?: (err: any, res: IGroupDocument[]) => void,
  ): Promise<IGroupDocument[]>;
  create(
    docs: IGroupTemplate[],
    options?: SaveOptions,
    callback?: (err: any, res: IGroupDocument[]) => void,
  ): Promise<IGroupDocument[]>;
  create(...docs: IGroupTemplate[]): Promise<IGroupDocument>;
  create(...docsWithCallback: IGroupTemplate[]): Promise<IGroupDocument>;

  compare(g1: IGroupDocument, g2: IGroupDocument): IGroupDocument;
}

const GroupSchema: Schema = new Schema({
  strategy: { type: Schema.Types.ObjectId, required: true },
  priority: { type: Number, required: true, default: 0 },
  meta: { type: [String], default: [] },
  shortDesc: { type: String },
  longDesc: { type: String },
});

GroupSchema.statics.compare = function(
  g1: IGroupDocument,
  g2: IGroupDocument,
): IGroupDocument {
  if (!isEqual(g2.strategy, g1.strategy))
    throw new Error("Groups of different strategies can't be compared");
  if (g2.priority > g1.priority) return g2;
  return g1;
};

GroupSchema.methods.compare = function(g: IGroupDocument): IGroupDocument {
  const self = this as IGroupDocument;
  if (!isEqual(g.strategy, self.strategy))
    throw new Error("Groups of different strategies can't be compared");
  if (g.priority > self.priority) return g;
  return self;
};
/*
  Group must be created by Strategy,
  Because Group must have dependent on Strategy but mongoDB does not support that relation pattern by native
*/
export const Group = model<IGroupDocument, IGroupModel>("Group", GroupSchema);
