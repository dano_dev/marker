import { Schema, Types } from "mongoose";
import { Action, IAction, IActionTemplate } from "./Action";
import { MonitorServerHelper } from "../common/helper";
import { IStrategyDocument } from "./Strategy";
import { delayActionsJobAttributesData } from "../job";
import { queue } from "../job";

export enum ScheduleStatus {
  Scheduled = "scheduled",
  UnScheduled = "unscheduled",
  Triggered = "triggered",
  Completed = "completed",
  Errored = "errored",
}

export interface IScheduleTemplate {
  status?: ScheduleStatus;
  actions?: Array<IActionTemplate>;
  scheduleDate: Date;
  shortDesc?: string;
  longDesc?: string;
}

export interface ISchedule extends Types.Subdocument {
  status: ScheduleStatus;
  actions: Types.DocumentArray<IAction>;
  scheduleDate: Date;
  shortDesc?: string;
  longDesc?: string;

  ownerDocument(): IStrategyDocument;
  parent(): IStrategyDocument;

  scheduleJob(strategyId: Types.ObjectId): Promise<void>;
  unScheduleJob(strategyId: Types.ObjectId): Promise<void>;
  triggerJob(): Promise<void>;
}

export const Schedule: Schema = new Schema(
  {
    status: {
      type: ScheduleStatus,
      required: true,
      default: ScheduleStatus.UnScheduled,
    },
    actions: { type: [Action], default: [] },
    scheduleDate: { type: Date, required: true },
    shortDesc: { type: String },
    longDesc: { type: String },
  },
  {
    timestamps: true,
  },
);

const monitorServerHelper = new MonitorServerHelper();

Schedule.methods.scheduleJob = async function(strategyId: Types.ObjectId) {
  const self: ISchedule = this;

  if (process.env.NODE_ENV === "production")
    await monitorServerHelper.addSchedule(strategyId, self);

  // only unscheduled status schedule can schedule itself
  if (self.status === ScheduleStatus.UnScheduled)
    self.status = ScheduleStatus.Scheduled;

  await self.parent().save();
};

Schedule.methods.unScheduleJob = async function(strategyId: Types.ObjectId) {
  const self: ISchedule = this;

  if (process.env.NODE_ENV === "production")
    await monitorServerHelper.deleteSchedule(strategyId, self);

  // already triggered job can't unscheduled
  if (self.status === ScheduleStatus.Scheduled)
    self.status = ScheduleStatus.UnScheduled;

  await self.parent().save();
};

Schedule.methods.triggerJob = async function() {
  const self: ISchedule = this;

  self.status = ScheduleStatus.Triggered;
  await self.parent().save();
  await queue.now<delayActionsJobAttributesData>("delayActions", {
    strategyId: self.parent()._id,
    scheduleId: self._id,
  });
};
