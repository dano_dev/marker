import { model, Schema, Document, Types, Model, SaveOptions } from "mongoose";
import { Group, IGroupDocument } from "./Group";
import { isEqual } from "lodash";

export interface ITargetTemplate {
  phone: string;
  groups?: Array<Types.ObjectId | IGroupDocument>;
  [key: string]: any;
}

export interface ITargetDocument extends ITargetTemplate, Document {
  groups: Array<Types.ObjectId | IGroupDocument>;

  getGroups(): Promise<Array<IGroupDocument>>;
  deleteGroup(
    ...groups: Array<IGroupDocument | Types.ObjectId>
  ): Promise<ITargetDocument>;
}

export interface ITargetModel extends Model<ITargetDocument> {
  pageSize: number;

  create(
    docs: ITargetTemplate[],
    callback?: (err: any, res: ITargetDocument[]) => void,
  ): Promise<ITargetDocument[]>;
  create(
    docs: ITargetTemplate[],
    options?: SaveOptions,
    callback?: (err: any, res: ITargetDocument[]) => void,
  ): Promise<ITargetDocument[]>;
  create(...docs: ITargetTemplate[]): Promise<ITargetDocument>;
  create(...docsWithCallback: ITargetTemplate[]): Promise<ITargetDocument>;

  findByPage(
    pageNum?: number,
    ...conditions: Array<any>
  ): Promise<ITargetDocument[]>;
}

const TargetSchema: Schema = new Schema(
  {
    _id: { type: String },
    groups: {
      type: [
        {
          type: Schema.Types.ObjectId,
          ref: "Group",
        },
      ],
      default: [],
    },
  },
  { strict: false },
);

TargetSchema.virtual("phone")
  .get(function() {
    return this._id;
  })
  .set(function(v) {
    this._id = v;
  });

TargetSchema.pre("save", async function() {
  const self = this as ITargetDocument;
  const groups = await self.getGroups();
  const cleanedGroups: Array<IGroupDocument> = [];

  let g: number | undefined;

  // Filter groups of the same strategy by priority
  for (let group of groups) {
    g = cleanedGroups.findIndex(v => isEqual(v.strategy, group.strategy));
    if (g === -1) {
      cleanedGroups.push(group);
      continue;
    }
    cleanedGroups[g] = cleanedGroups[g].compare(group);
  }

  self.groups = cleanedGroups;
});

TargetSchema.statics.pageSize = 10;
TargetSchema.statics.findByPage = async function(
  pageNum: number = 1,
  ...conditions: Array<any>
) {
  const cls: ITargetModel = this;
  return cls
    .find(...conditions)
    .limit(cls.pageSize)
    .skip(cls.pageSize * (pageNum - 1))
    .exec();
};

/**
 * @define Get populated groups
 * @return Array of group instance
 */
TargetSchema.methods.getGroups = async function(): Promise<
  Array<IGroupDocument>
> {
  let self: ITargetDocument = this;

  if (self.groups.length === 0) return self.groups as Array<IGroupDocument>;
  else if (self.groups[0] instanceof Group)
    return self.groups as Array<IGroupDocument>;

  await self.populate("groups").execPopulate();
  return self.groups as Array<IGroupDocument>;
};

/**
 * delete matched group in target.groups
 * @param groups {Array<IGroupDocument| Types.ObjectId>}
 */
TargetSchema.methods.deleteGroup = async function(
  ...groups: Array<IGroupDocument | Types.ObjectId>
): Promise<ITargetDocument> {
  const self = this as ITargetDocument;

  self.groups = (await self.getGroups()).filter(
    g =>
      !groups.some(gg =>
        isEqual(
          g._id,
          gg instanceof Types.ObjectId ? gg : (gg as IGroupDocument)._id,
        ),
      ),
  );
  return self.save();
};

export const Target = model<ITargetDocument, ITargetModel>(
  "Target",
  TargetSchema,
);
