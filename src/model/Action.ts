import { Document, Schema, Types } from "mongoose";
import { ISchedule } from "./Schedule";
import { IStrategyDocument } from "./Strategy";
import { isEqual } from "lodash";

export enum ActionStatus {
  Waiting = "waiting",
  Pending = "pending",
  Error = "error",
  Complete = "complete",
}

export interface IActionTemplate {
  // group: Types.ObjectId;
}

export interface IAction extends Types.Subdocument {
  group: Types.ObjectId;
  method: string;
  status: ActionStatus;
  shortDesc?: string;
  longDesc?: string;

  ownerDocument(): IStrategyDocument;
  parent(): ISchedule;
  action(): Promise<any>;
}

export const Action = new Schema(
  {
    group: { type: Schema.Types.ObjectId, required: true, ref: "Group" },
    method: { type: String, required: true },
    status: {
      type: ActionStatus,
      required: true,
      default: ActionStatus.Waiting,
    },
    shortDesc: { type: String },
    longDesc: { type: String },
  },
  {
    timestamps: true,
  },
);

Action.pre("save", async function() {
  const self: any = this;
  const s: IStrategyDocument = self.ownerDocument();

  if (!(await s.isInMyGroups(self.group)))
    throw new Error(
      `action's group "${self.group}" is not in strategy "${s._id}"!`,
    );

  const sc: ISchedule = self.parent();
  if (
    sc.actions.some(v => v.group !== self.group && isEqual(v.group, self.group))
  )
    throw new Error(
      `action's group "${self.group}" is already exist in schedule "${sc._id}"!`,
    );
});

Action.methods.action = async function() {
  const self: IAction = this;
  console.log(self.method);
};
