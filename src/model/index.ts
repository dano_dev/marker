import * as Koa from "koa";
import * as mongoose from "mongoose";

export async function initApp(
  app: Koa<Koa.DefaultState, Koa.MarkerContext>,
): Promise<mongoose.Mongoose> {
  let uri: string;
  (<any>mongoose).Promise = global.Promise;

  uri =
    process.env.MONGO_URI ||
    "mongodb://root:ilovedano@mongo:27017/marker?authSource=admin";
  app.context.db = await mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    poolSize: 20,
  });

  app.context.db.connection.on("connected", () => {
    console.info("Mongoose default connection open to " + uri);
  });

  app.context.db.connection.on("error", (err: string) => {
    console.error("Mongoose default connection error: " + err);
  });

  app.context.db.connection.on("disconnected", () => {
    console.warn("Mongoose default connection disconnected");
  });

  process.on("SIGINT", async () => {
    await app.context.db.connection.close(() => {
      console.info(
        "Mongoose default connection closed through app termination",
      );
      process.exit(0);
    });
  });

  return app.context.db;
}

// Export submodules
export * from "./Strategy";
export * from "./Target";
export * from "./Group";
export * from "./Schedule";
export { IAction } from "./Action";
