import "reflect-metadata";
import { Service } from "typedi";

import {
  EventActionType,
  EventHookMeta,
  EventReceivePayload,
  getEventActionType,
} from "../common";
import { IGroupDocument, Strategy, Target } from "../model";
import { BadRequestError } from "routing-controllers";

@Service()
export class EventHookService {
  private async getMatchedMataGroups(
    meta: EventHookMeta,
  ): Promise<IGroupDocument[]> {
    const activatedStrategies = await Strategy.find({ isActivated: true });
    const groups: Array<IGroupDocument> = [];

    await Promise.all(
      activatedStrategies.map(async s => {
        groups.push(
          ...(await s.getGroups()).filter(g => g.meta.some(m => m === meta)),
        );
      }),
    );
    return groups;
  }

  private async deleteMetaGroup(
    meta: EventHookMeta,
    payload: EventReceivePayload,
  ) {
    const target = await Target.findById(payload.phone);
    if (!target) {
      await Target.create(payload);
      return;
    }

    const groups: Array<IGroupDocument> = await this.getMatchedMataGroups(meta);
    await target.deleteGroup(...groups);

    return;
  }

  private async addMetaGroup(
    meta: EventHookMeta,
    payload: EventReceivePayload,
  ) {
    const result = {
      isCreated: false,
    };

    let target = await Target.findById(payload.phone);
    if (!target) {
      target = new Target(payload);
      result.isCreated = true;
    }

    const groups: Array<IGroupDocument> = await this.getMatchedMataGroups(meta);
    target.groups.push(...groups);
    await target.save();

    return result;
  }

  async receiveEvent(meta: EventHookMeta, payload: EventReceivePayload) {
    if (payload.groups)
      throw new BadRequestError("Event Payload can't have groups!");

    switch (getEventActionType(meta)) {
      case EventActionType.Add:
        return this.addMetaGroup(meta, payload);
      case EventActionType.Delete:
        return this.deleteMetaGroup(meta, payload);
    }
  }
}
