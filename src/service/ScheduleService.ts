import "reflect-metadata";
import { Service } from "typedi";

import { ISchedule, ScheduleStatus, Strategy } from "../model";
import { Types } from "mongoose";
import { isEqual } from "lodash";
import { BadRequestError, NotFoundError } from "routing-controllers";
import { ScheduleCreatePayload, ScheduledJobMonitorStatus } from "../common";
import { ActionStatus } from "../model/Action";

@Service()
export class ScheduleService {
  async getAll(id: Types.ObjectId): Promise<ISchedule[]> {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);
    return s.schedules;
  }

  async getOne(id: Types.ObjectId, sid: Types.ObjectId): Promise<ISchedule> {
    let sc: ISchedule;
    try {
      sc = await Strategy.getSchedule(id, sid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }
    return sc;
  }

  async create(
    id: Types.ObjectId,
    schedule: ScheduleCreatePayload,
  ): Promise<ISchedule> {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);
    try {
      return (await s.addSchedule(schedule))[0];
    } catch (e) {
      throw new BadRequestError(e.message);
    }
  }

  async delete(id: Types.ObjectId, sid: Types.ObjectId): Promise<ISchedule> {
    let sc: ISchedule;
    try {
      sc = await Strategy.getSchedule(id, sid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }

    sc.remove();
    await sc.parent().save();
    return sc;
  }

  async update(
    id: Types.ObjectId,
    sid: Types.ObjectId,
    scheduleDate: Date,
  ): Promise<ISchedule | undefined> {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);

    const sc = s.schedules.find(v => isEqual(v._id, sid));
    if (!sc) return;
    sc.scheduleDate = scheduleDate;
    await s.save();

    return sc;
  }

  async trigger(id: Types.ObjectId, sid: Types.ObjectId) {
    let sc: ISchedule;
    try {
      sc = await Strategy.getSchedule(id, sid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }

    await sc.triggerJob();
    return { progress: `/strategies/${id}/schedules/${sid}/progress` };
  }

  async progress(id: Types.ObjectId, sid: Types.ObjectId) {
    let sc: ISchedule;
    try {
      sc = await Strategy.getSchedule(id, sid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }

    const result = {
      status: ScheduledJobMonitorStatus.Complete,
      message: "",
    };

    switch (sc.status) {
      case ScheduleStatus.UnScheduled:
        result.status = ScheduledJobMonitorStatus.Error;
        result.message = "unscheduled schedule!";
        break;
      case ScheduleStatus.Scheduled:
        result.status = ScheduledJobMonitorStatus.Error;
        result.message = "scheduled but not triggered schedule!";
        break;
      case ScheduleStatus.Triggered:
        result.status = ScheduledJobMonitorStatus.Pending;
        break;
      case ScheduleStatus.Completed:
        result.status = ScheduledJobMonitorStatus.Complete;
        for (let a of sc.actions) {
          if (a.status === ActionStatus.Waiting) {
            result.status = ScheduledJobMonitorStatus.Error;
            result.message = "triggered but action is not pending!";
            break;
          } else if (a.status === ActionStatus.Error) {
            result.status = ScheduledJobMonitorStatus.Error;
            result.message = "something wrongs in actions!";
            break;
          } else if (a.status === ActionStatus.Pending) {
            result.status = ScheduledJobMonitorStatus.Pending;
          }
        }
        break;
      case ScheduleStatus.Errored:
        result.status = ScheduledJobMonitorStatus.Error;
        result.message = "error occurred while triggering!";
        break;
    }
    return result;
  }
}
