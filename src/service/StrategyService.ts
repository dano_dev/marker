import "reflect-metadata";
import { Service } from "typedi";

import { PaginatedResponse, StrategyCreatePayload } from "../common";
import { IStrategyDocument, IStrategyTemplate, Strategy } from "../model";
import { Types } from "mongoose";

@Service()
export class StrategyService {
  async getAllByPage(page: number, query?: any): Promise<PaginatedResponse> {
    const qs = await Strategy.findByPage(page, query);
    return {
      size: qs.length,
      page: page,
      pageSize: Strategy.pageSize,
      values: qs,
    };
  }

  async getOne(id: Types.ObjectId): Promise<IStrategyDocument | null> {
    return Strategy.findById(id);
  }

  async create(strategy: StrategyCreatePayload): Promise<IStrategyDocument> {
    return Strategy.create(strategy);
  }

  async activate(id: Types.ObjectId): Promise<IStrategyDocument | null> {
    const s = await Strategy.findById(id);
    if (!s) return s;

    return s.activate();
  }

  async deactivate(id: Types.ObjectId): Promise<IStrategyDocument | null> {
    const s = await Strategy.findById(id);
    if (!s) return s;

    return s.deactivate();
  }
}
