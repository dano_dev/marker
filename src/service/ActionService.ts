import "reflect-metadata";
import { Service } from "typedi";

import { IAction, ISchedule, Strategy } from "../model";
import { Types } from "mongoose";
import { BadRequestError, NotFoundError } from "routing-controllers";
import { ActionCreatePayload } from "../common";

@Service()
export class ActionService {
  async getAll(id: Types.ObjectId, sid: Types.ObjectId): Promise<IAction[]> {
    let sc: ISchedule;
    try {
      sc = await Strategy.getSchedule(id, sid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }
    return sc.actions;
  }

  async getOne(
    id: Types.ObjectId,
    sid: Types.ObjectId,
    aid: Types.ObjectId,
  ): Promise<IAction> {
    let a: IAction;
    try {
      a = await Strategy.getAction(id, sid, aid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }
    return a;
  }

  async delete(
    id: Types.ObjectId,
    sid: Types.ObjectId,
    aid: Types.ObjectId,
  ): Promise<IAction> {
    let a: IAction;
    try {
      a = await Strategy.getAction(id, sid, aid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }

    a.remove();
    await a.ownerDocument().save();
    return a;
  }

  async create(
    id: Types.ObjectId,
    sid: Types.ObjectId,
    action: ActionCreatePayload,
  ): Promise<IAction> {
    let sc: ISchedule;
    try {
      sc = await Strategy.getSchedule(id, sid);
    } catch (e) {
      throw new NotFoundError(e.message);
    }

    try {
      sc.actions.push(action);
      await sc.parent().save();
      return sc.actions[sc.actions.length - 1];
    } catch (e) {
      throw new BadRequestError(e.message);
    }
  }
}
