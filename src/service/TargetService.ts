import "reflect-metadata";
import { Service } from "typedi";

import {
  PaginatedResponse,
  TargetBulkCreatePayload,
  TargetPayload, withLockAcquired
} from "../common";
import { ITargetDocument, Target } from "../model";
import { Types } from "mongoose";
import { BadRequestError } from "routing-controllers";
import { GroupService } from "./GroupService";

@Service()
export class TargetService {
  bulkSize: number = 2000;

  async getAllByPage(
    page: number,
    groupId?: Types.ObjectId,
    query?: any,
  ): Promise<PaginatedResponse> {
    const qs = await Target.findByPage(page, {
      ...query,
      group: groupId,
    });
    return {
      size: qs.length,
      page: page,
      pageSize: Target.pageSize,
      values: qs,
    };
  }

  /**
   * validate target by target service's policy
   * @param target
   * @param duplicationCheck, optional, if true throw http error when target id is duplicated
   */
  static async validateTarget(
    target: TargetPayload,
    duplicationCheck: boolean = true,
  ): Promise<ITargetDocument | null> {
    if (target.groups && !(await GroupService.isValidGroupId(...target.groups)))
      throw new BadRequestError(`${target.groups} have invalid group id!`);

    const t = await Target.findById(target.phone);
    if (duplicationCheck && t)
      throw new BadRequestError(`${target.phone} is already exist target!`);

    return t;
  }

  async getOne(phone: string): Promise<ITargetDocument | null> {
    return Target.findById(phone);
  }

  async create(payload: TargetPayload): Promise<ITargetDocument> {
    await TargetService.validateTarget(payload);
    return Target.create(payload);
  }

  async update(payload: TargetPayload): Promise<ITargetDocument | undefined> {
    const value = await TargetService.validateTarget(payload, false);
    if (!value) return;

    delete payload.phone;
    for (let p in payload) value.set(p, payload[p]);

    return value.save();
  }

  async createOrUpdateBulk(payload: TargetBulkCreatePayload): Promise<any> {
    if (payload.targets.length > this.bulkSize)
      throw new BadRequestError(
        `body size lager than supported size ${this.bulkSize}`,
      );

    if (payload.targets.length <= 0)
      throw new BadRequestError(
        `empty targets! requested target -> ${payload.targets}`,
      );

    if (
      payload.groups &&
      !(await GroupService.isValidGroupId(...payload.groups))
    )
      throw new BadRequestError(`${payload.groups} have invalid group id!`);

    const cleanedTargets: Array<TargetPayload> = await Promise.all(
      payload.targets.map(async v => {
        if (!v.phone)
          throw new BadRequestError(
            `${JSON.stringify(v)} dose not have required argument "phone"!`,
          );
        v.groups = payload.groups;
        return v;
      }),
    );

    const session = await Target.db.startSession();

    try {
      await session.withTransaction(async () => {
        await Promise.all(cleanedTargets.map( async ct => {
          let t = await Target.findById(ct.phone).session(session);

          if (!t) {
            await Target.create([ct], {session: session});
            return;
          }

          if (ct.groups) t.groups.push(...ct.groups);
          await t.save({session: session});
        }));
      }, {
        readConcern: { level: 'snapshot' },
        writeConcern: { w: 'majority' }
      });
    } catch (e) {
      throw new BadRequestError(e);
    } finally {
      session.endSession();
    }

    return {'ok': 1}
  }

  async addGroup(payload: TargetPayload): Promise<ITargetDocument | undefined> {
    const t = await TargetService.validateTarget(payload, false);
    if (!t) return;

    t.groups.push(...payload.groups!);
    return t.save();
  }

  async removeGroup(
    payload: TargetPayload,
  ): Promise<ITargetDocument | undefined> {
    const t = await TargetService.validateTarget(payload, false);
    if (!t) return;

    return t.deleteGroup(...payload.groups!);
  }
}
