import "reflect-metadata";
import { Service } from "typedi";

import { Group, IGroupDocument, Strategy } from "../model";
import { Types } from "mongoose";
import { isEqual } from "lodash";
import { NotFoundError } from "routing-controllers";
import { GroupCreatePayload } from "../common";

@Service()
export class GroupService {
  static async isValidGroupId(...ids: Array<Types.ObjectId>) {
    let isIt = true;
    for (let id of ids) {
      if (!(await Group.exists({ _id: id }))) {
        isIt = false;
        break;
      }
    }
    return isIt;
  }

  async getAll(id: Types.ObjectId): Promise<IGroupDocument[]> {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);
    return s.getGroups();
  }

  async getOne(
    id: Types.ObjectId,
    gid: Types.ObjectId,
  ): Promise<IGroupDocument | undefined> {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);
    return (await s.getGroups()).find(v => isEqual(v._id, gid));
  }

  async create(
    id: Types.ObjectId,
    group: GroupCreatePayload,
  ): Promise<IGroupDocument> {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);
    return s.createGroup(group);
  }

  async update(
    id: Types.ObjectId,
    gid: Types.ObjectId,
    group: GroupCreatePayload,
  ) {
    const s = await Strategy.findById(id);
    if (!s) throw new NotFoundError(`No matched strategy to id ${id}`);

    const g = (await s.getGroups()).find(v => isEqual(v._id, gid));
    if (!g) return;

    return g.update(group);
  }
}
