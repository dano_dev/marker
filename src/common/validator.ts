import { ObjectId } from "mongodb";
import { isNumeric, parseDate } from "./util";
import { BadRequestError } from "routing-controllers";
import { EventHookMeta } from "./index";

export interface IValidatorProps {
  type: ValidatableType | [ValidatableType];
  required?: boolean;
  default?: any;
}

export enum ValidatableType {
  String = "string",
  Number = "number",
  PositiveNumber = "positive number",
  PhoneNum = "phone number",
  Date = "date",
  ObjectId = "objectId",
  Array = "array",
  Boolean = "boolean",
  Object = "object",
  EventHookMeta = "event hook meta",
}

/**
 * validator create function that throw errors
 * @param type: {ValidatableType}
 */
const validator = (type: ValidatableType) => {
  switch (type) {
    case ValidatableType.String:
      return (value: any): string | never => {
        if (typeof value !== "string") throw new TypeError();
        return value;
      };
    case ValidatableType.Number:
      return (value: any): number | never => {
        if (typeof value === "number") {
          return value;
        } else if (typeof value == "string" && isNumeric(value)) {
          return Number(value);
        } else throw new TypeError();
      };
    case ValidatableType.PositiveNumber:
      return (value: any): number | never => {
        if (typeof value === "number") {
          value = value as number;
        } else if (typeof value == "string" && isNumeric(value)) {
          value = Number(value) as number;
        } else throw new TypeError();

        if (value <= 0) throw new TypeError();
        return value as number;
      };
    case ValidatableType.PhoneNum:
      return (value: any): string | never => {
        if (typeof value === "number") {
          return String(value);
        } else if (typeof value == "string" && isNumeric(value)) {
          return value;
        } else throw new TypeError();
      };
    case ValidatableType.Date:
      return (value: any): Date | never => {
        if (typeof value === "string") value = parseDate(value);
        else throw new TypeError();

        if (!value) throw new TypeError();
        return value;
      };
    case ValidatableType.ObjectId:
      return (value: any) => new ObjectId(value);
    case ValidatableType.Array:
      return (value: any): Array<any> | never => {
        if (Array.isArray(value)) return value;
        throw new TypeError();
      };
    case ValidatableType.Boolean:
      return (value: any): boolean | never => {
        if (typeof value === "boolean") return value;
        throw new TypeError();
      };
    case ValidatableType.Object:
      return (value: any): object | never => {
        if (typeof value === "object") return value;
        throw new TypeError();
      };
    case ValidatableType.EventHookMeta:
      return (value: any): EventHookMeta | never => {
        if (typeof value === "string") value = value.toUpperCase();
        if (EventHookMeta[value]) return EventHookMeta[value];
        throw new TypeError();
      };
    default:
      throw new TypeError();
  }
};

/**
 * validate should catch general error and rethrow http errors
 * @param subject: anything witch validated
 * @param desc: detail spec of validation
 * @param isStrict: if true, drop properties not described in desc
 */
export const validate = <T>(
  subject: any,
  desc: { [p: string]: IValidatorProps },
  isStrict: boolean = true,
): T | never => {
  const enhancedBody = isStrict ? ({} as T) : ({ ...subject } as T);

  for (let key in desc) {
    let props: IValidatorProps = desc[key];

    if (key in subject) {
      if (Array.isArray(props.type)) {
        const type = props.type[0];
        if (!Array.isArray(subject[key]))
          throw new BadRequestError(
            `Invalid type of ${key}, "${subject[key]}" is not array`,
          );

        try {
          const values: Array<any> = [];

          for (let v of subject[key]) values.push(validator(type)(v));

          enhancedBody[key] = values;
        } catch (e) {
          throw new BadRequestError(
            `Invalid type of ${key}, "${subject[key]}" is not array of ${type}`,
          );
        }
      } else {
        try {
          enhancedBody[key] = validator(props.type)(subject[key]);
        } catch (e) {
          throw new BadRequestError(
            `Invalid type of ${key}, "${subject[key]}" is not ${props.type}`,
          );
        }
      }
    } else if (props.default) enhancedBody[key] = props.default;
    else if (props.required)
      throw new BadRequestError(`${key} is required but got nothing!`);
  }
  return enhancedBody;
};
