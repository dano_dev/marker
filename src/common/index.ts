import { Types } from "mongoose";

export * from "./validator";
export * from "./util";

export type PaginatedResponse = {
  size: number;
  page: number;
  pageSize: number;
  values: Array<any>;
};

export type TargetPayload = {
  phone: string;
  groups?: Array<Types.ObjectId>;
  [key: string]: any;
};

export type TargetUpdatePayload = {
  phone?: string;
  groups?: Array<Types.ObjectId>;
  [key: string]: any;
};

export type TargetGroupPatchPayload = {
  op: string;
  value: Array<Types.ObjectId>;
};

export type TargetBulkCreatePayload = {
  targets: Array<any>;
  groups?: Array<Types.ObjectId>;
};

export type StrategyCreatePayload = {
  shortDesc?: string;
  longDesc?: string;
};

export type GroupCreatePayload = {
  priority?: number;
  meta?: Array<EventHookMeta>;
  shortDesc?: string;
  longDesc?: string;
};

export type ScheduleCreatePayload = {
  scheduleDate: Date;
  shortDesc?: string;
  longDesc?: string;
};

export type ActionCreatePayload = {
  group: Types.ObjectId;
  method: string;
  shortDesc?: string;
  longDesc?: string;
};

export type EventReceivePayload = {
  phone: string;
  [key: string]: any;
};

export enum EventHookMeta {
  PAYMENT = "payment",
}

export enum EventActionType {
  Add,
  Delete,
}

export function getEventActionType(meta: EventHookMeta): EventActionType {
  switch (meta) {
    case EventHookMeta.PAYMENT:
      return EventActionType.Delete;
  }
}

export enum ScheduledJobMonitorStatus {
  Waiting = "WAITING",
  Pending = "PENDING",
  Complete = "COMPLETE",
  Error = "ERROR",
}
