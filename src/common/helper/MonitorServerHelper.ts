import fetch from "node-fetch";
import { ISchedule } from "../../model";
import { Types } from "mongoose";
import * as moment from "moment";

export class MonitorServerHelper {
  private url = process.env.MONITOR_URL || 'http://127.0.0.1:5000/marker';

  private dateToFormattedStr(date: Date): string {
    return moment(date).format('YYYY-MM-DDTHH:mm:ssZZ')
  }

  public async getSchedules() {
    const method = 'GET';
    const end_point = '/schedules';

    const res = await fetch(this.url + end_point, {
      method: method
    });
    if (res.status !== 200)
      throw new Error(res.statusText);

    return res.json()
  }

  public async addSchedule(strategyId: Types.ObjectId, schedule: ISchedule) {
    const method = 'POST';
    const end_point = '/schedules';

    const body = {
      strategy_id: strategyId,
      schedule_id: schedule._id,
      schedule_date: this.dateToFormattedStr(schedule.scheduleDate)
    };

    const res = await fetch(this.url + end_point, {
      method: method,
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    });
    if (res.status !== 201)
      throw new Error(res.statusText);

    return res.json()
  }

  public async deleteSchedule(strategyId: Types.ObjectId, schedule: ISchedule) {
    const method = 'DELETE';
    const end_point = '/schedules';

    const body = {
      strategy_id: strategyId,
      schedule_id: schedule._id,
    };

    const res = await fetch(this.url + end_point, {
      method: method,
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    });
    if (res.status !== 200 && res.status !== 404)
      throw new Error(res.statusText);

    return res.json()
  }

}