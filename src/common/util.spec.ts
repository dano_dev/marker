import { isNumeric, parseDate } from "./util";
import { expect } from "chai";

describe("Utils", () => {
  describe("isNumeric", () => {
    it("should validate string argument is numeric", async () => {
      expect(isNumeric()).to.be.false;
      expect(isNumeric("s123245fg")).to.be.false;
      expect(isNumeric("sdasd")).to.be.false;

      expect(isNumeric("123")).to.be.true;
    });
  });

  describe("parseDate", () => {
    it("should parse string argument to Date", async () => {
      expect(parseDate()).to.be.undefined;

      expect(parseDate("2019-12-17 10-51:55")).to.be.undefined;
      expect(parseDate("2019-12-17 10-51")).to.be.undefined;
      expect(parseDate("2019-12-17 10:51")).to.be.undefined;
      expect(parseDate("2019-12-17 10:51:55")).to.be.instanceOf(Date);
      expect(parseDate("2019-12-17 10:51:55+0900")).to.be.instanceOf(Date);

      expect(parseDate("2019-12-17T10-51:55")).to.be.undefined;
      expect(parseDate("2019-12-17T10-51")).to.be.undefined;
      expect(parseDate("2019-12-17T10:51")).to.be.undefined;
      expect(parseDate("2019-12-17T10:51:55")).to.be.instanceOf(Date);
      expect(parseDate("2019-12-17T10:51:55+0900")).to.be.instanceOf(Date);
    });
  });
});
