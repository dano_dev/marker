import { IValidatorProps, ValidatableType, validate } from "./validator";

import { expect } from "chai";

describe("Validators", () => {
  describe("validate", () => {
    it("should validate required desc", async () => {
      const desc = {
        id: { type: ValidatableType.String, required: true },
      } as { [p: string]: IValidatorProps };
      const case1 = {
        id: "1asd3masd",
      };
      const case2 = {
        cd: "asds",
      };

      expect(validate(case1, desc)).is.deep.equals({
        id: "1asd3masd",
      });
      expect(() => validate(case2, desc)).to.be.throws();
    });

    it("should throw error when unexpected type is entered", async () => {
      const desc = {
        id: { type: ValidatableType.ObjectId, required: true },
      } as { [p: string]: IValidatorProps };
      const case1 = {
        id: "12dsds",
      };

      expect(() => validate(case1, desc)).to.be.throws();
    });

    it("should generate default value if not exist", () => {
      const desc = {
        id: { type: ValidatableType.String, required: true, default: "1322" },
      } as { [p: string]: IValidatorProps };
      const case1 = {
        id: "1asd3masd",
      };
      const case2 = {};

      expect(validate(case1, desc)).is.deep.equals({
        id: "1asd3masd",
      });
      expect(validate(case2, desc)).is.deep.equals({
        id: "1322",
      });
    });

    it("should cast type if available", async () => {});

    it("should drop not described properties if isStrict option is true", async () => {
      const desc = {
        id: { type: ValidatableType.String },
        p2: { type: ValidatableType.Number, default: 123 },
      } as { [p: string]: IValidatorProps };
      const case1 = {
        id: "1asd3masd",
        asc: "bbadc",
      };

      expect(validate(case1, desc)).is.deep.equals({
        id: "1asd3masd",
        p2: 123,
      });
    });

    it("should not drop not described properties if isStrict option is false", async () => {
      const desc = {
        id: { type: ValidatableType.String },
        p2: { type: ValidatableType.Number, default: 123 },
      } as { [p: string]: IValidatorProps };
      const case1 = {
        id: "1asd3masd",
        asc: "bbadc",
      };

      expect(validate(case1, desc, false)).is.deep.equals({
        id: "1asd3masd",
        asc: "bbadc",
        p2: 123,
      });
    });

    it("should validate array of type", () => {
      const desc = {
        ids: { type: [ValidatableType.String] },
      } as { [p: string]: IValidatorProps };
      const case1 = {
        ids: ["1asd3masd", "2321sd"],
      };
      const case2 = {
        ids: ["1asd3masd", 231],
      };
      const case3 = {
        ids: "1asd3masd",
      };

      expect(validate(case1, desc)).is.deep.equals({
        ids: ["1asd3masd", "2321sd"],
      });
      expect(() => validate(case2, desc)).to.be.throws();
      expect(() => validate(case3, desc)).to.be.throws();
    });
  });
});
