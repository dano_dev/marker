import { ObjectId } from "mongodb";
import { WebClient, MessageAttachment } from "@slack/web-api";
import * as moment from "moment";
import { lock, release } from "./redisLock";

const token = process.env.SLACK_TOKEN;
const slack = new WebClient(token);

export enum SlackChannel {
  AlertTest = "#알림_테스트",
  MarkerAlert = "#마커_알림",
}

export enum SlackAlertLevel {
  Info = "good",
  Warning = "warning",
  Danger = "danger",
}

const supportedDateFormat = [
  "YYYY-MM-DDTHH:mm:ss",
  "YYYY-MM-DDTHH:mm:ssZZ",
  "YYYY-MM-DD HH:mm:ss",
  "YYYY-MM-DD HH:mm:ssZZ",
];

export function isNumeric(str?: string): boolean {
  if (!str) return false;
  return /^\d+$/.test(str);
}

export function isObjectId(value?: any) {
  if (!value) return false;

  try {
    let a = new ObjectId(value);
  } catch (e) {
    return false;
  }
  return true;
}

export function parseDate(value?: string): Date | undefined {
  if (!value) return;

  let m: moment.Moment | null = null;
  for (let format of supportedDateFormat) {
    m = moment(value, format, true);
    if (m.isValid()) break;
    else m = null;
  }

  if (!m) return;
  return m.toDate();
}

export async function sendSlackMsg(
  channel: SlackChannel,
  body: {
    title: string;
    text?: string;
    fields?:
      | {
          title: string;
          value: string;
          short?: boolean;
        }[]
      | { [p: string]: string };
  },
  alertLevel: SlackAlertLevel = SlackAlertLevel.Info,
): Promise<void> {
  if (process.env.NODE_ENV !== "production") channel = SlackChannel.AlertTest;

  const attachment = {
    color: alertLevel,
  } as MessageAttachment;

  if (body.text) attachment.text = body.text;
  if (body.fields) {
    if (Array.isArray(body.fields)) {
      attachment.fields = body.fields;
    } else {
      attachment.fields = [];
      for (let p in body.fields)
        attachment.fields.push({
          title: p,
          value: body.fields[p],
          short: true,
        });
    }
  }

  const res = await slack.chat.postMessage({
    channel: channel,
    text: `:loud_sound: *${body.title}* :loud_sound:`,
    username: "Monitoring Emperor",
    icon_emoji: ":king_jaekun:",
    attachments: [attachment],
  });

  if (!res.ok) throw new Error(res.error);
}

export async function withLockAcquired(
  lockKey: string,
  fn: () => Promise<any>,
  duration: number = 10
): Promise<any> {
  const isLockAcquired = await lock(lockKey, duration);
  if (!isLockAcquired) throw new Error(`Lock acquire error, duration -> ${duration}`);

  let re: any;
  try {
    re = await fn();
  } catch (e) {
    throw new Error(`Blocked job failed! error -> ${e}`);
  } finally {
    await release(lockKey);
  }

  return re;
}

export function getRandomString() {
  return Math.random().toString(36).substr(2, 5);
}