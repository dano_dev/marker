import * as redis from "redis";
import { promisify } from "util";

const redisUrl = process.env.REDIS_URL || 'redis://127.0.0.1:6379/1';

const client = redis.createClient(redisUrl);
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);

export const lock = async (key: string, dur: number): Promise<boolean> => {
  const result = await setAsync(`LOCK_${key}`, 1, "NX", "EX", dur);
  return result === "OK";
};

export const release = async (key: string) => {
  await delAsync(`LOCK_${key}`);
};

export const setCache = async (key: string, data: string, dur: number) => {
  const result = await setAsync(`CACHE_${key}`, data, "NX", "EX", dur);
  return result === "OK";
};

export const getCache = async (key: string): Promise<string> => {
  return await getAsync(`CACHE_${key}`);
};
