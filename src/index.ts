import "dotenv/config";
import "source-map-support/register";

import * as Koa from 'koa';
import * as app from './app';

const appName: string = 'Marker';
const nodeEnv: string = process.env.NODE_ENV || 'development';
const port: string = process.env.SERVER_PORT || '3000';

app.createApp(appName)
  .then(async (a: Koa<Koa.DefaultState, Koa.MarkerContext>) => {
    a.listen(port, async () => {
      console.info(`${appName} started in NODE_ENV → ${nodeEnv}`);
      console.info(`Koa started: port → ${port}`);
    });
  }).catch(e => {
    console.error(e);
    process.exit(1);
  });
