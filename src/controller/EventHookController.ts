import { Inject } from "typedi";
import {
  Authorized,
  JsonController,
  OnUndefined,
  Put,
} from "routing-controllers";

import { ValidatedBody, ValidatedParam } from "../decorator";
import { EventHookMeta, EventReceivePayload, ValidatableType } from "../common";
import { EventHookService } from "../service";

@JsonController("/event")
export class EventHookController {
  @Inject()
  service: EventHookService;

  @Authorized()
  @Put("/:meta/")
  @OnUndefined(204)
  async receiveEvent(
    @ValidatedParam("meta", {
      type: ValidatableType.EventHookMeta,
      required: true,
    })
    meta: EventHookMeta,
    @ValidatedBody(
      {
        phone: { type: ValidatableType.PhoneNum, required: true },
      },
      false,
    )
    body: EventReceivePayload,
  ) {
    return this.service.receiveEvent(meta, body);
  }
}
