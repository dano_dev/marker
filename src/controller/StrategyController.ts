import { Types } from "mongoose";
import { Inject } from "typedi";
import {
  Authorized,
  Get,
  HttpCode,
  JsonController,
  OnNull,
  Post,
} from "routing-controllers";

import {
  ValidatedBody,
  ValidatedBodyParam,
  ValidatedParam,
  ValidatedQueryParam,
} from "../decorator";
import { StrategyCreatePayload, ValidatableType } from "../common";
import { StrategyService } from "../service";

@JsonController("/strategies")
export class StrategyController {
  @Inject()
  service: StrategyService;

  @Authorized()
  @Get("/")
  async getAll(
    @ValidatedQueryParam("page", { type: ValidatableType.PositiveNumber })
    page: number = 1,
    @ValidatedBodyParam("query", { type: ValidatableType.Object })
    query?: any,
  ) {
    return this.service.getAllByPage(page, query);
  }

  @Authorized()
  @Get("/:id")
  @OnNull(404)
  async get(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
  ) {
    return this.service.getOne(id);
  }

  @Authorized()
  @HttpCode(201)
  @Post("/")
  async create(
    @ValidatedBody({
      shortDesc: { type: ValidatableType.String },
      longDesc: { type: ValidatableType.String },
    })
    strategy: StrategyCreatePayload,
  ) {
    return this.service.create(strategy);
  }

  @Authorized()
  @Post("/:id/activate")
  @OnNull(404)
  async activate(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
  ) {
    return this.service.activate(id);
  }

  @Authorized()
  @Post("/:id/deactivate")
  @OnNull(404)
  async deactivate(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
  ) {
    return this.service.deactivate(id);
  }
}
