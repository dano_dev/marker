import {
  Authorized,
  Delete,
  Get,
  HttpCode,
  JsonController,
  OnUndefined,
  Post,
  Put,
} from "routing-controllers";
import { Inject } from "typedi";

import {
  ValidatedBody,
  ValidatedBodyParam,
  ValidatedParam,
} from "../decorator";
import { ScheduleCreatePayload, ValidatableType } from "../common";
import { Types } from "mongoose";
import { ScheduleService } from "../service/ScheduleService";

@JsonController("/strategies/:id/schedules")
export class ScheduleController {
  @Inject()
  service: ScheduleService;

  @Authorized()
  @Get("/")
  async getAll(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
  ) {
    return this.service.getAll(id);
  }

  @Authorized()
  @Get("/:sid")
  async get(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
  ) {
    return this.service.getOne(id, sid);
  }

  @Authorized()
  @Delete("/:sid")
  async delete(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
  ) {
    return this.service.delete(id, sid);
  }

  @Authorized()
  @Put("/:sid")
  @OnUndefined(204)
  async update(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
    @ValidatedBodyParam("scheduleDate", {
      type: ValidatableType.Date,
      required: true,
    })
    scheduleDate: Date,
  ) {
    return this.service.update(id, sid, scheduleDate);
  }

  @Authorized()
  @HttpCode(201)
  @Post("/")
  async create(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedBody({
      scheduleDate: { type: ValidatableType.Date, required: true },
      shortDesc: { type: ValidatableType.String },
      longDesc: { type: ValidatableType.String },
    })
    schedule: ScheduleCreatePayload,
  ) {
    return this.service.create(id, schedule);
  }

  @Authorized()
  @HttpCode(202)
  @Post("/:sid/trigger")
  async trigger(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
  ) {
    return this.service.trigger(id, sid);
  }

  @Authorized()
  @Get("/:sid/progress")
  async progress(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
  ) {
    return this.service.progress(id, sid);
  }
}
