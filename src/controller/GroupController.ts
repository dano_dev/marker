import {
  Authorized,
  Get,
  HttpCode,
  JsonController,
  OnUndefined,
  Post,
  Put,
} from "routing-controllers";
import { Inject } from "typedi";

import { ValidatedBody, ValidatedParam } from "../decorator";
import { GroupCreatePayload, ValidatableType } from "../common";
import { Types } from "mongoose";
import { GroupService } from "../service";

@JsonController("/strategies/:id/groups")
export class GroupController {
  @Inject()
  service: GroupService;

  @Authorized()
  @Get("/")
  async getAll(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
  ) {
    return this.service.getAll(id);
  }

  @Authorized()
  @Get("/:gid")
  @OnUndefined(404)
  async get(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("gid", { type: ValidatableType.ObjectId, required: true })
    gid: Types.ObjectId,
  ) {
    return this.service.getOne(id, gid);
  }

  @Authorized()
  @Put("/:gid")
  @OnUndefined(204)
  async update(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("gid", { type: ValidatableType.ObjectId, required: true })
    gid: Types.ObjectId,
    @ValidatedBody({
      priority: { type: ValidatableType.Number },
      meta: { type: [ValidatableType.EventHookMeta] },
      shortDesc: { type: ValidatableType.String },
      longDesc: { type: ValidatableType.String },
    })
    group: GroupCreatePayload,
  ) {
    return this.service.update(id, gid, group);
  }

  @Authorized()
  @HttpCode(201)
  @Post("/")
  async create(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedBody({
      priority: { type: ValidatableType.Number },
      meta: { type: [ValidatableType.EventHookMeta] },
      shortDesc: { type: ValidatableType.String },
      longDesc: { type: ValidatableType.String },
    })
    group: GroupCreatePayload,
  ) {
    return this.service.create(id, group);
  }
}
