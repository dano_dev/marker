import { Authorized, Get, JsonController } from "routing-controllers";

interface HealthCheckResult {
  error: number;
  unavailable: Array<string>;
  message: string;
}

@JsonController()
export class MonitorController {
  @Get("/heartbeat")
  heartbeat() {
    return "doki doki";
  }

  @Authorized()
  @Get("/healthcheck")
  healthcheck() {
    const result: HealthCheckResult = {
      error: 0,
      unavailable: [],
      message: "doki doki",
    };
    try {
    } catch (e) {
      console.log(e);
      result.error += 1;
      result.unavailable.push("database");
    }
    return result;
  }
}
