import "reflect-metadata";

import * as Koa from "koa";
import { Container } from "typedi";
import { Action, useKoaServer, useContainer } from "routing-controllers";

export async function initApp(
  app: Koa<Koa.DefaultState, Koa.MarkerContext>,
  authorizationChecker?: (
    action: Action,
    roles: string[],
  ) => Promise<boolean> | boolean,
) {
  useContainer(Container);

  const { StrategyController } = await import("./StrategyController");
  const { GroupController } = await import("./GroupController");
  const { ScheduleController } = await import("./ScheduleController");
  const { TargetController } = await import("./TargetController");
  const { MonitorController } = await import("./MonitorController");
  const { EventHookController } = await import("./EventHookController");
  const { ActionController } = await import("./ActionController");

  return useKoaServer(app, {
    controllers: [
      StrategyController,
      GroupController,
      ScheduleController,
      TargetController,
      MonitorController,
      EventHookController,
      ActionController,
    ],

    defaultErrorHandler: false,
    classTransformer: false,
    validation: false,

    authorizationChecker: authorizationChecker,
  });
}
