import {
  Authorized,
  Delete,
  Get,
  HttpCode,
  JsonController,
  Post,
} from "routing-controllers";
import { Inject } from "typedi";

import { ValidatedBody, ValidatedParam } from "../decorator";
import { ActionCreatePayload, ValidatableType } from "../common";
import { Types } from "mongoose";
import { ActionService } from "../service/ActionService";

@JsonController("/strategies/:id/schedules/:sid/actions")
export class ActionController {
  @Inject()
  service: ActionService;

  @Authorized()
  @Get("/")
  async getAll(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
  ) {
    return this.service.getAll(id, sid);
  }

  @Authorized()
  @Get("/:aid")
  async get(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
    @ValidatedParam("aid", { type: ValidatableType.ObjectId, required: true })
    aid: Types.ObjectId,
  ) {
    return this.service.getOne(id, sid, aid);
  }

  @Authorized()
  @Delete("/:aid")
  async delete(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
    @ValidatedParam("aid", { type: ValidatableType.ObjectId, required: true })
    aid: Types.ObjectId,
  ) {
    return this.service.delete(id, sid, aid);
  }

  @Authorized()
  @HttpCode(201)
  @Post("/")
  async create(
    @ValidatedParam("id", { type: ValidatableType.ObjectId, required: true })
    id: Types.ObjectId,
    @ValidatedParam("sid", { type: ValidatableType.ObjectId, required: true })
    sid: Types.ObjectId,
    @ValidatedBody({
      group: { type: ValidatableType.ObjectId, required: true },
      method: { type: ValidatableType.String, required: true },
      shortDesc: { type: ValidatableType.String },
      longDesc: { type: ValidatableType.String },
    })
    action: ActionCreatePayload,
  ) {
    return this.service.create(id, sid, action);
  }
}
