import { Types } from "mongoose";
import {
  Authorized,
  BadRequestError,
  Get,
  HttpCode,
  JsonController,
  OnNull,
  OnUndefined,
  Patch,
  Post,
  Put,
} from "routing-controllers";
import { Inject } from "typedi";
import {
  ValidatedBody,
  ValidatedBodyParam,
  ValidatedParam,
  ValidatedQueryParam,
} from "../decorator";
import {
  TargetBulkCreatePayload,
  TargetPayload,
  TargetGroupPatchPayload,
  TargetUpdatePayload,
  ValidatableType,
} from "../common";
import { TargetService } from "../service";

@JsonController("/targets")
export class TargetController {
  @Inject()
  service: TargetService;

  @Authorized()
  @Get("/")
  async getAll(
    @ValidatedQueryParam("page", { type: ValidatableType.PositiveNumber })
    page: number = 1,
    @ValidatedBodyParam("group", { type: ValidatableType.ObjectId })
    group?: Types.ObjectId,
    @ValidatedBodyParam("query", { type: ValidatableType.Object })
    query?: any,
  ) {
    return this.service.getAllByPage(page, group, query);
  }

  @Authorized()
  @Get("/:phone")
  @OnNull(404)
  async get(
    @ValidatedParam("phone", { type: ValidatableType.PhoneNum, required: true })
    phone: string,
  ) {
    return this.service.getOne(phone);
  }

  @Authorized()
  @HttpCode(201)
  @Post("/")
  async create(
    @ValidatedBody(
      {
        phone: { type: ValidatableType.PhoneNum, required: true },
        groups: { type: [ValidatableType.ObjectId] },
      },
      false,
    )
    body: TargetPayload,
  ) {
    return this.service.create(body);
  }

  @Authorized()
  @Post("/bulk")
  async createOrUpdateBulk(
    @ValidatedBody({
      targets: { type: ValidatableType.Array, required: true },
      groups: { type: [ValidatableType.ObjectId] },
    })
    body: TargetBulkCreatePayload,
  ) {
    return this.service.createOrUpdateBulk(body);
  }

  @Authorized()
  @Put("/:phone")
  @OnUndefined(204)
  async update(
    @ValidatedParam("phone", { type: ValidatableType.PhoneNum, required: true })
    phone: string,
    @ValidatedBody(
      {
        groups: { type: [ValidatableType.ObjectId] },
      },
      false,
    )
    body: TargetUpdatePayload,
  ) {
    body.phone = phone;
    return this.service.update(body as TargetPayload);
  }

  @Authorized()
  @Patch("/:phone/groups")
  @OnUndefined(404)
  async addGroups(
    @ValidatedParam("phone", { type: ValidatableType.PhoneNum, required: true })
    phone: string,
    @ValidatedBody({
      op: { type: ValidatableType.String, required: true },
      value: { type: [ValidatableType.ObjectId], required: true },
    })
    body: TargetGroupPatchPayload,
  ) {
    switch (body.op) {
      case "add":
        return this.service.addGroup({ phone: phone, groups: body.value });
      case "remove":
        return this.service.removeGroup({ phone: phone, groups: body.value });
      default:
        throw new BadRequestError(`Unsupported operation "${body.op}"!`);
    }
  }
}
