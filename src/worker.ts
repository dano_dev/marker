import "dotenv/config";
import "source-map-support/register";

import { initApp } from "./job";

initApp().then( agenda => {
  console.info(`worker starts!`);
}).catch(e => {
  console.error(e);
  process.exit(1);
});

