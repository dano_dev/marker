import { ScheduleStatus, Strategy } from "../model";
import * as Agenda from "agenda";
import { ActionStatus } from "../model/Action";
import { Types } from "mongoose";

export type delayActionsJobAttributesData = {
  strategyId: Types.ObjectId;
  scheduleId: Types.ObjectId;
};

export type processActionJobAttributesData = {
  strategyId: Types.ObjectId;
  scheduleId: Types.ObjectId;
  actionId: Types.ObjectId;
};

export default function(agenda: Agenda) {
  agenda.define<processActionJobAttributesData>("processAction", async job => {
    const a = await Strategy.getAction(
      job.attrs.data.strategyId,
      job.attrs.data.scheduleId,
      job.attrs.data.actionId,
    );

    try {
      await a.action();
      a.status = ActionStatus.Complete;
    } catch (e) {
      a.status = ActionStatus.Error;
    }

    await a.ownerDocument().save();
  });

  agenda.define<delayActionsJobAttributesData>("delayActions", async job => {
    const sc = await Strategy.getSchedule(
      job.attrs.data.strategyId,
      job.attrs.data.scheduleId,
    );

    try {
      await Promise.all(
        sc.actions.map(async v => {
          v.status = ActionStatus.Pending;
          await job.agenda.now<processActionJobAttributesData>(
            "processAction",
            {
              strategyId: job.attrs.data.strategyId,
              scheduleId: job.attrs.data.scheduleId,
              actionId: v._id,
            },
          );
        }),
      );
      sc.status = ScheduleStatus.Completed;
    } catch (e) {
      sc.status = ScheduleStatus.Errored;
    }

    await sc.parent().save();
  });
}
