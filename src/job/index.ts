import * as Agenda from "agenda";
import * as os from "os";
import * as mongodb from "mongodb";

import { AgendaConfiguration } from "agenda";

export let queue: Agenda;

export async function initApp(db?: mongodb.Db) {
  const jobTypes = process.env.JOB_TYPES
    ? process.env.JOB_TYPES.split(",")
    : [];
  let connectionOpts: AgendaConfiguration;

  if (db) {
    connectionOpts = {
      mongo: db,
    };
  } else {
    connectionOpts = {
      db: {
        address:
          process.env.MONGO_URI ||
          "mongodb://root:ilovedano@mongo:27017/marker?authSource=admin",
        options: {
          useUnifiedTopology: true,
          useNewUrlParser: true,
          autoReconnect: undefined,
          reconnectTries: undefined,
          reconnectInterval: undefined,
        },
      },
    };
  }

  const agenda = new Agenda(connectionOpts);
  agenda.name(os.hostname + "-" + process.pid);

  await Promise.all(
    jobTypes.map(async type => {
      const { default: task } = await import("./" + type);
      task(agenda);
    }),
  );

  if (jobTypes.length) await agenda.start();

  async function graceful() {
    await agenda.stop();
    process.exit(0);
  }

  process.on("SIGTERM", graceful);
  process.on("SIGINT", graceful);

  queue = agenda;
  return agenda;
}

export {
  delayActionsJobAttributesData,
  processActionJobAttributesData,
} from "./delayActions";
