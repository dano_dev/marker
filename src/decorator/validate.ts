import * as Koa from "koa";

import { createParamDecorator } from "routing-controllers";
import { IValidatorProps, validate } from "../common";

export function ValidatedParam<T>(name: string, props: IValidatorProps) {
  return createParamDecorator({
    required: !!(props && props.required),
    value: action => {
      const subject = (action.context as Koa.MarkerContext).params;
      return validate<{ [k: string]: T }>(subject, { [name]: props })[name];
    },
  });
}

export function ValidatedQueryParam<T>(name: string, props: IValidatorProps) {
  return createParamDecorator({
    required: !!(props && props.required),
    value: action => {
      const subject = (action.context as Koa.MarkerContext).request.query;
      return validate<{ [k: string]: T }>(subject, { [name]: props })[name];
    },
  });
}

export function ValidatedBodyParam<T>(name: string, props: IValidatorProps) {
  return createParamDecorator({
    required: !!(props && props.required),
    value: action => {
      const subject = (action.context as Koa.MarkerContext).request.body;
      return validate<{ [k: string]: T }>(subject, { [name]: props })[name];
    },
  });
}

export function ValidatedBody<T>(
  desc: { [p: string]: IValidatorProps },
  isStrict: boolean = true,
) {
  return createParamDecorator({
    value: action => {
      const subject = (action.context as Koa.MarkerContext).request.body;
      return validate<T>(subject, desc, isStrict);
    },
  });
}
