import "dotenv/config";
import * as Koa from "koa";
import { Action, HttpError } from "routing-controllers";
import { sendSlackMsg, SlackAlertLevel, SlackChannel } from "../common";

export async function apiStatusCheck(ctx: Koa.MarkerContext, next: Koa.Next) {
  try {
    await next();
  } catch (err) {
    ctx.log.debug("query -> ", ctx.request.query);
    ctx.log.debug("body -> ", ctx.request.body);
    ctx.log.debug("params -> ", ctx.params);

    if (err instanceof HttpError) {
      ctx.status = err.httpCode;
      ctx.body = err.message;
      ctx.log.warn(
        `HTTP ERROR!\n\nstatus code → ${ctx.status}\nmessage → ${ctx.body}`,
      );
      await sendSlackMsg(
        SlackChannel.MarkerAlert,
        {
          title: "API exception caught",
          fields: { exception: err.name, description: err.message },
        },
        SlackAlertLevel.Warning,
      );
    } else {
      ctx.status = 500;
      ctx.body = err.message;
      ctx.log.error(`Unhandled ERROR!\n\nmessage → ${ctx.body}`);
      await sendSlackMsg(
        SlackChannel.MarkerAlert,
        {
          title: "Unhandled API exception caught",
          text: err.message,
        },
        SlackAlertLevel.Danger,
      );
    }

    ctx.app.emit("error", err, ctx);
  }
}

export function apiKeyAuthentication(action: Action, roles: string[]) {
  const apiKey = action.context.get("x-authorization");
  return apiKey === process.env.SERVICE_API_KEY;
}
