import * as Koa from 'koa';

import * as mongoose from 'mongoose';

declare module 'koa' {
  let log: (message: string, ctx?: MarkerContext) => void;

  interface Request {
    originalUrl: string;
    ip: string;
    body?: any;
    rawBody: string;
  }

  interface MarkerContext extends Koa.Context{
    appName: string;
    log: {
      debug: (...messages: Array<any>) => void,
      info: (...messages: Array<any>) => void,
      warn: (...messages: Array<any>) => void,
      error: (...messages: Array<any>) => void,
    };
    params?: {[key: string]: string};
    db: typeof mongoose;
    request: Koa.Request;

    /**
     * Throw an error with `msg` and optional `status`
     * defaulting to 500. Note that these are user-level
     * errors, and the message may be exposed to the client.
     *
     *    this.throw(403)
     *    this.throw('name required', 400)
     *    this.throw(400, 'name required')
     *    this.throw('something exploded')
     *    this.throw(new Error('invalid'), 400);
     *    this.throw(400, new Error('invalid'));
     *
     * See: https://github.com/jshttp/http-errors
     */
    throw(message: string, code?: number, properties?: {}): never;
    throw(status: number): never;
    throw(status: number, message: string): never;
    throw(...properties: Array<number | string | {}>): never;
  }
}