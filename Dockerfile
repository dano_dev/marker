FROM node:10-alpine as dev

LABEL maintainer="jaegeon <zezaoh@gmail.com>"

RUN apk add tzdata && \
    cp /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
    echo "Asia/Seoul" > /etc/timezone && \
    apk del tzdata

ENV NODE_ENV=development
WORKDIR /app

COPY . .
RUN yarn install && yarn build

CMD [ "yarn", "start:dev" ]

FROM node:10-alpine as prod

LABEL maintainer="jaegeon <zezaoh@gmail.com>"

RUN apk add tzdata && \
    cp /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
    echo "Asia/Seoul" > /etc/timezone && \
    apk del tzdata

ENV NODE_ENV=production
WORKDIR /app

COPY --from=dev /app/dist dist
COPY --from=dev /app/package.json package.json
COPY --from=dev /app/yarn.lock yarn.lock
COPY --from=dev /app/.env .env

RUN yarn install --production=true

CMD [ "yarn", "start:prod" ]