import { expect, use } from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { EventHookService, TargetService } from "../src/service";
import {
  IGroupDocument,
  IStrategyDocument,
  Strategy,
  Target,
} from "../src/model";
import { ObjectId } from "mongodb";
import { compareDocument } from "./index.spec";
import { EventHookMeta } from "../src/common";

use(chaiAsPromised);

describe("Services", () => {
  describe("TargetService", () => {
    const strategies: Array<IStrategyDocument> = [];
    const groups: Array<IGroupDocument> = [];
    const service = new TargetService();

    before(async function() {
      this.timeout(60000);
      // 15 num of mock strategies, each of strategy have two group
      for (let i = 0; i < 15; i++) {
        let s = await Strategy.create({});
        groups.push(
          await s.createGroup({
            priority: 1,
          }),
        );
        groups.push(
          await s.createGroup({
            priority: 3,
          }),
        );
        strategies.push(s);
      }
    });

    after(async function() {
      this.timeout(60000);

      await Promise.all(strategies.map(v => v.remove()));
      await Promise.all(groups.map(g => g.remove()));
    });

    describe("validateTarget", () => {
      before(async () => {
        await Target.create({
          phone: "821000000000",
        });
        await Target.create({
          phone: "821000000001",
        });
        await Target.create({
          phone: "821000000002",
        });
      });

      after(async function() {
        this.timeout(60000);

        await Promise.all((await Target.find()).map(v => v.remove()));
      });

      it("should throw errors if there is duplicated phone num", async () => {
        // duplicated
        await expect(
          TargetService.validateTarget({
            phone: "821087823829",
          }),
        ).eventually.null;
        // not duplicated
        await expect(
          TargetService.validateTarget({
            phone: "821000000001",
          }),
        ).eventually.rejected;
      });

      it("should throw errors if there is invalid groups id", async () => {
        // valid ids
        await expect(
          TargetService.validateTarget({
            phone: "821087823829",
            groups: [groups[0]._id, groups[2]._id, groups[4]._id],
          }),
        ).eventually.null;
        // invalid ids
        await expect(
          TargetService.validateTarget({
            phone: "821087823829",
            groups: [
              groups[0]._id,
              new ObjectId("5dedda8d9b875902ee31d4ac"),
              groups[4]._id,
            ],
          }),
        ).eventually.rejected;
      });

      it("should return Target object matched with param.phone if duplicationCheck option is false", async () => {
        // when duplicated phone num exist
        expect(
          (
            await TargetService.validateTarget(
              {
                phone: "821000000001",
              },
              false,
            )
          )?.phone,
        ).is.eq("821000000001");

        // when duplicated phone num doesn't exist
        expect(
          await TargetService.validateTarget(
            {
              phone: "821087823829",
            },
            false,
          ),
        ).is.null;
      });
    });

    describe("getOne", () => {});
    describe("create", () => {});

    describe("update", () => {
      beforeEach(async () => {
        await Target.create({
          phone: "821000000003",
          groups: [groups[0], groups[2], groups[4], groups[6]],
        });
      });

      afterEach(async () => {
        await Promise.all((await Target.find()).map(v => v.remove()));
      });

      it("should update target with new properties", async () => {
        await service.update({
          phone: "821000000003",
          test1: "23",
          test2: 13,
        });
        const t = await Target.findById("821000000003");

        expect(
          compareDocument(await t!.getGroups(), [
            groups[0],
            groups[2],
            groups[4],
            groups[6],
          ]),
        ).to.be.true;
        expect(t?.get("test1")).is.eq("23");
        expect(t?.get("test2")).is.eq(13);
      });

      it("should update target with group priority validation", async () => {
        await service.update({
          phone: "821000000003",
          groups: [groups[0]._id, groups[1]._id, groups[2]._id, groups[3]._id],
        });

        expect(
          compareDocument(
            await (await Target.findById("821000000003"))!.getGroups(),
            [groups[1], groups[3]],
          ),
        ).to.be.true;
      });
    });

    describe("createOrUpdateBulk", () => {
      beforeEach(async () => {
        await Target.create({
          phone: "821000000001",
          groups: [groups[0], groups[2], groups[4], groups[6]],
        });
      });

      afterEach(async () => {
        await Promise.all((await Target.find()).map(v => v.remove()));
      });

      it("should throw errors if body size lager than supported size", async () => {
        const mock: Array<any> = [];
        for (let i = 0; i < service.bulkSize + 5; i++) mock.push(i);

        await expect(
          service.createOrUpdateBulk({
            groups: [],
            targets: mock,
          }),
        ).eventually.rejected;
      });

      it("should throw errors if group id is invalid", async () => {
        await expect(
          service.createOrUpdateBulk({
            groups: [groups[0]._id, new ObjectId("5dedda8d9b875902ee31d4ac")],
            targets: [],
          }),
        ).eventually.rejected;
      });

      it("should throw errors if exist that doesn't have phone as property in targets", async () => {
        await expect(
          service.createOrUpdateBulk({
            groups: [groups[0]._id, new ObjectId("5dedda8d9b875902ee31d4ac")],
            targets: [
              {
                phone: "821087823829",
              },
              {
                test: 2,
              },
            ],
          }),
        ).eventually.rejected;
      });

      it("should create target if bulked target is not exist", async () => {
        await service.createOrUpdateBulk({
          groups: [groups[0]._id, groups[2]._id],
          targets: [
            {
              phone: "821087823829",
            },
          ],
        });

        expect(
          compareDocument(
            await (await Target.findById("821087823829"))!.getGroups(),
            [groups[0], groups[2]],
          ),
        ).to.be.true;
      });

      it("should update target if bulked target is exist", async () => {
        await service.createOrUpdateBulk({
          groups: [groups[1]._id, groups[3]._id],
          targets: [
            {
              phone: "821000000001",
            },
          ],
        });

        expect(
          compareDocument(
            await (await Target.findById("821000000001"))!.getGroups(),
            [groups[1], groups[3], groups[4], groups[6]],
          ),
        ).to.be.true;
      });

      it("should handle both targets that already exist and not", async () => {
        await service.createOrUpdateBulk({
          groups: [groups[1]._id, groups[3]._id],
          targets: [
            {
              phone: "821000000001",
            },
            {
              phone: "821087823829",
            },
          ],
        });

        expect(
          compareDocument(
            await (await Target.findById("821000000001"))!.getGroups(),
            [groups[1], groups[3], groups[4], groups[6]],
          ),
        ).to.be.true;
        expect(
          compareDocument(
            await (await Target.findById("821087823829"))!.getGroups(),
            [groups[1], groups[3]],
          ),
        ).to.be.true;
      });
    });

    describe("addGroup", () => {});

    describe("removeGroup", () => {
      beforeEach(async () => {
        await Target.create({
          phone: "821000000000",
          groups: [groups[0], groups[2], groups[4], groups[6]],
        });
      });

      afterEach(async () => {
        await Promise.all((await Target.find()).map(v => v.remove()));
      });

      it("should remove group in groups at target matched with param.groups", async () => {
        expect(
          await compareDocument(
            await (await service.removeGroup({
              phone: "821000000000",
              groups: [groups[0]._id, groups[4]._id],
            }))!.getGroups(),
            [groups[2], groups[6]],
          ),
        ).to.be.true;

        expect(
          await compareDocument(
            await (await service.removeGroup({
              phone: "821000000000",
              groups: [groups[2]._id],
            }))!.getGroups(),
            [groups[6]],
          ),
        ).to.be.true;
      });

      it("should accept empty list of groups", async () => {
        expect(
          await compareDocument(
            await (await service.removeGroup({
              phone: "821000000000",
              groups: [],
            }))!.getGroups(),
            [groups[0], groups[2], groups[4], groups[6]],
          ),
        ).to.be.true;
      });
    });
  });

  describe("EventHookService", () => {
    const strategies: Array<IStrategyDocument> = [];
    const groups: Array<IGroupDocument> = [];
    const service = new EventHookService();

    before(async function() {
      this.timeout(60000);
      // 15 num of mock strategies, each of strategy have two group
      for (let i = 0; i < 15; i++) {
        let s = await Strategy.create({});
        groups.push(
          await s.createGroup({
            priority: 1,
            meta: [EventHookMeta.PAYMENT],
          }),
        );
        groups.push(
          await s.createGroup({
            priority: 3,
          }),
        );
        strategies.push(s);
      }
    });

    after(async function() {
      this.timeout(60000);

      await Promise.all(strategies.map(v => v.remove()));
      await Promise.all(groups.map(g => g.remove()));
    });

    describe("receiveEvent", () => {
      beforeEach(async () => {
        await Target.create({
          phone: "821010100001",
          groups: [groups[0], groups[1], groups[2], groups[3]],
        });
      });

      afterEach(async function() {
        this.timeout(60000);

        await Promise.all((await Target.find()).map(v => v.remove()));
      });

      it("should handle 'Add' EventActionType of EventHookMeta", async () => {});

      it("should handle 'Delete' EventActionType of EventHookMeta", async () => {
        await service.receiveEvent(EventHookMeta.PAYMENT, {
          phone: "821010100001",
        });

        expect(
          compareDocument(
            await (await Target.findById("821010100001"))!.getGroups(),
            [groups[1], groups[3]],
          ),
        ).to.be.true;
      });

      it("should throw errors if payload contains groups", async () => {
        await expect(
          service.receiveEvent(EventHookMeta.PAYMENT, {
            phone: "821087823829",
            groups: [1, 2, 3],
          }),
        ).eventually.rejected;
      });
    });
  });
});
