import {
  Strategy,
  IStrategyDocument,
  IGroupDocument,
  Target,
  ITargetDocument,
} from "../src/model";

import { expect } from "chai";
import { compareDocument } from "./index.spec";

describe("Models", () => {
  describe("Strategy", () => {
    const strategies: Array<IStrategyDocument> = [];

    before(async function() {
      this.timeout(60000);

      // 15개의 mock strategies
      for (let i = 0; i < 15; i++) strategies.push(await Strategy.create({}));
    });

    after(async function() {
      this.timeout(60000);

      await Promise.all(strategies.map(s => s.remove()));
    });

    // Strategy는 Group을 생성할 수 있고, 생성된 Group은 해당 Strategy에 종속적이된다.
    it("should creates a group that depends on itself.", async () => {
      const mockStrategy: IStrategyDocument = strategies[0];
      // console.log(mockStrategy);

      const mockParam = {
        shortDesc: "hihi",
        longDesc: "hello",
      };

      const group = await mockStrategy.createGroup(mockParam);
      expect(group).is.not.null;
      expect(group.shortDesc).is.eq(mockParam.shortDesc);
      expect(group.longDesc).is.eq(mockParam.longDesc);
      expect(group.strategy).is.eq(mockStrategy._id);
    });

    // Strategy는 Schedule을 생성하고 추가할 수 있다.
    it("should generates a schedule and add to schedules", () => {});

    // Strategy는 Schedule의 id를 기반으로 Action을 만들고 해당 Schedule에 추가할 수 있다.
    // 생성된 Action은 반드시 Strategy에 종속된 Group만을 지칭할 수 있다.
    it("should generates a schedule that refers to the group that depends on itself.", () => {});

    describe("findByPage", () => {
      it("should return searched query result as paged format", async () => {
        expect(
          compareDocument(await Strategy.findByPage(), strategies.slice(0, 10)),
        ).to.be.true;

        expect(
          compareDocument(
            await Strategy.findByPage(1),
            strategies.slice(0, 10),
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await Strategy.findByPage(2),
            strategies.slice(10, 15),
          ),
        ).to.be.true;

        expect(compareDocument(await Strategy.findByPage(3), [])).to.be.true;

        await expect(Strategy.findByPage(-1)).eventually.rejected;
      });
    });
  });

  describe("Target", () => {
    let strategy: IStrategyDocument;
    const groups: Array<IGroupDocument> = [];

    const strategies: Array<IStrategyDocument> = [];

    before(async function() {
      this.timeout(60000);

      strategy = await Strategy.create({});
      groups.push(await strategy.createGroup({ priority: 5 })); // idx: 0
      groups.push(await strategy.createGroup({ priority: 4 })); // idx: 1
      groups.push(await strategy.createGroup({ priority: 3 })); // idx: 2
      groups.push(await strategy.createGroup({ priority: 5 })); // idx: 3

      // 15 num of mock strategies, each of strategy have two group
      for (let i = 0; i < 15; i++) {
        let s = await Strategy.create({});
        groups.push(
          await s.createGroup({
            priority: 1,
          }),
        );
        groups.push(
          await s.createGroup({
            priority: 3,
          }),
        );
        strategies.push(s);
      }
    });

    after(async function() {
      this.timeout(60000);

      await strategy.remove();
      await Promise.all(groups.map(v => v.remove()));
      await Promise.all(strategies.map(v => v.remove()));
    });

    afterEach(async () => {
      await Promise.all((await Target.find()).map(v => v.remove()));
    });

    it("should throw errors if there is duplicated phone number", async () => {
      await Target.create({
        phone: "821087823829",
      });

      await expect(
        Target.create({
          phone: "821087823829",
        }),
      ).eventually.rejected;
    });

    it("should store duplicate groups in order of priority.", async () => {
      // 동일한 우선순위일 경우 먼저 저장된 그룹을 남김
      expect(
        compareDocument(
          await (
            await Target.create({
              phone: "821087823829",
              groups: [groups[0], groups[3]],
            })
          ).getGroups(),
          [groups[0]],
        ),
      ).to.be.true;
      expect(
        compareDocument(
          await (
            await Target.create({
              phone: "821077823829",
              groups: [groups[3], groups[0]],
            })
          ).getGroups(),
          [groups[3]],
        ),
      ).to.be.true;

      // 우선순위가 가장 높은 그룹만을 남김
      expect(
        compareDocument(
          await (
            await Target.create({
              phone: "821087812829",
              groups: [groups[0], groups[1], groups[2]],
            })
          ).getGroups(),
          [groups[0]],
        ),
      ).to.be.true;
      expect(
        compareDocument(
          await (
            await Target.create({
              phone: "821087852829",
              groups: [groups[2], groups[1]],
            })
          ).getGroups(),
          [groups[1]],
        ),
      ).to.be.true;
    });

    describe("findByPage", () => {
      const targets: Array<ITargetDocument> = [];

      before(async () => {
        // 15개의 mock targets
        for (let i = 0; i < 15; i++)
          targets.push(await Target.create({ phone: i + "" }));
      });

      after(async function() {
        this.timeout(60000);

        await Promise.all(targets.map(s => s.remove()));
      });

      it("should return searched query result as paged format", async () => {
        expect(compareDocument(await Target.findByPage(), targets.slice(0, 10)))
          .to.be.true;

        expect(
          compareDocument(await Target.findByPage(1), targets.slice(0, 10)),
        ).to.be.true;

        expect(
          compareDocument(await Target.findByPage(2), targets.slice(10, 15)),
        ).to.be.true;

        expect(compareDocument(await Target.findByPage(3), [])).to.be.true;

        await expect(Target.findByPage(-1)).eventually.rejected;
      });
    });

    describe("deleteGroup", () => {
      beforeEach(async () => {
        await Target.create({
          phone: "821000000009",
          groups: [groups[4], groups[6], groups[8], groups[10]],
        });
      });

      afterEach(async () => {
        await Promise.all((await Target.find()).map(v => v.remove()));
      });

      it("should delete matched groups when arguments are Group document", async () => {
        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup(
                groups[4],
                groups[6],
              )
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup(
                groups[5],
                groups[7],
              )
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup()
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;
      });

      it("should delete matched groups when arguments are ObjectId", async () => {
        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup(
                groups[4]._id,
                groups[6]._id,
              )
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup(
                groups[5]._id,
                groups[7]._id,
              )
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup()
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;
      });

      it("should delete matched groups when arguments are combined with ObjectId and Group", async () => {
        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup(
                groups[4],
                groups[6]._id,
              )
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup(
                groups[5]._id,
                groups[7],
              )
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;

        expect(
          compareDocument(
            await (
              await (await Target.findById("821000000009"))!.deleteGroup()
            ).getGroups(),
            [groups[8], groups[10]],
          ),
        ).to.be.true;
      });
    });
  });
});
