import "dotenv/config";

import * as mongoose from "mongoose";
import { Document } from "mongoose";
import { isEqual } from "lodash";

export const compareDocument = (
  s1s: Array<Document>,
  s2s: Array<Document>,
): boolean => {
  const n = s1s.length;
  if (n !== s2s.length) {
    console.log(JSON.stringify(s1s));
    console.log(JSON.stringify(s2s));
    return false;
  }

  for (let i = 0; i < n; i++)
    if (!isEqual(s1s[i]._id, s2s[i]._id)) {
      console.log(i);
      console.log(s1s[i]._id);
      console.log(s2s[i]._id);

      console.log();
      console.log(JSON.stringify(s1s));
      console.log(JSON.stringify(s2s));
      return false;
    }

  return true;
};

before(async () => {
  (<any>mongoose).Promise = global.Promise;
  const uri =
    process.env.MONGO_TEST_URI ||
    "mongodb://root:ilovedano@mongo:27017/marker-test?authSource=admin";
  return await mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    poolSize: 20,
  });
});

after(async () => {
  await mongoose.connection.db.dropDatabase();
  return await mongoose.connection.close();
});
